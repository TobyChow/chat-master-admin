## Chat-Master-Admin

<p>
    <a href="#联系我们"><img src="https://img.shields.io/badge/%E5%85%AC%E4%BC%97%E5%8F%B7-%E5%A4%A7%E5%B8%88%E5%AD%A6Java-blue" alt="公众号"></a>
</p>

> 声明：此项目只发布于码云，基于 MIT 协议，免费且作为开源学习使用。并且不会有任何形式的卖号、付费服务、讨论群、讨论组等行为。谨防受骗。

# 项目简介
ChatMASTERAdmin，使用VUE、Element-Ui框架搭建的ChatMARTER后台管理项目，用于配制使用模型、密钥及助手等。已对接模型有：ChatGPT、文心一言、通义千问、讯飞星火。
搭配后台[chat-master](https://gitee.com/panday94/chat-master)使用，可以快速搭建属于自己的ChatOwner后台。

## 开发

```bash
# 克隆项目
git clone https://gitee.com/panday94/chat-master-admin

# 进入项目目录
cd chat-master-admin

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```

浏览器访问 http://localhost:2345

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 演示图

<table>
<table>
    <tr>
        <td><img src="./docs/1.jpg"/></td>
        <td><img src="./docs/2.jpg"/></td>
    </tr>
    <tr>
        <td><img style="max-width: 100%" src="./docs/assistant.jpg" alt="微信" /></td>
        <td><img src="./docs/openkey.jpg"/></td>
    </tr>
	<tr>
        <td><img src="./docs/3.jpg"/></td>
    </tr>
</table>

## 参与贡献

贡献之前请先阅读 [贡献指南](./CONTRIBUTING.md)

感谢所有做过贡献的人!

## 特别鸣谢

* [RuoYi](https://gitee.com/y_project/RuoYi)

## 赞助

如果你觉得这个项目对你有帮助，并且情况允许的话，可以给我一点点支持，总之非常感谢支持～

<div style="display: flex; gap: 20px;">
	<div style="text-align: center">
		<img style="max-width: 100%" src="./docs/wechat.jpg" alt="微信" />
		<p>WeChat Pay</p>
	</div>
</div>

## 联系我们

<div style="display: flex; gap: 20px;">
	<div style="text-align: center">
		<img style="max-width: 100%" src="./docs/mpqrcode.jpg" alt="公众号" />
		<p>公众号</p>
	</div>
	<div style="text-align: center">
		<img style="max-width: 100%" src="./docs/wxcode.jpg" alt="微信" />
		<p>添加微信，加入交流群</p>
	</div>
</div>

## License
MIT © [Master](./license)
